﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Carta : MonoBehaviour{
    public JogoUI jogoUI;
    public Tema tema;
    public int idCarta;
    public Image imagem;

    public void MostrarCarta() {
        Sprite cartaMostrada = tema.cartasTipos[idCarta];
        imagem.sprite = cartaMostrada;
    }

    public void EsconderCarta() {
        imagem.sprite = tema.cartaEscondida;
    }

    public void RetirarCarta() {
        Color color = Color.white;
        color.a = 0.2f;
        imagem.color = color;

        GetComponent<Button>().enabled = false;
    }

    public void BotaoSelecionarCarta() {
        jogoUI.MostrarCarta(this);
    }
}
