﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuUI : MonoBehaviour{
    public Menu menu;

    public Toggle isSomAtivado;

    [Header("Painel Ajustes")]
    public GameObject painelAjustes;
    public InputField nome1;
    public InputField nome2;
    public InputField nome3;
    public InputField nome4;

    [Space(10)]
    public Image botaoFacil;
    public Image botaoMedio;
    public Image botaoDificil;

    void Start() {
        painelAjustes.SetActive(false);
    }

    public void escolherNumJogadores(Slider slider) {
        int value = (int)slider.value;

        if (value == 1) {
            nome2.gameObject.SetActive(false);
            nome3.gameObject.SetActive(false);
            nome4.gameObject.SetActive(false);
        }else if (value == 2) {
            nome2.gameObject.SetActive(true);
            nome3.gameObject.SetActive(false);
            nome4.gameObject.SetActive(false);
        }else if (value == 3) {
            nome2.gameObject.SetActive(true);
            nome3.gameObject.SetActive(true);
            nome4.gameObject.SetActive(false);
        } else {
            nome2.gameObject.SetActive(true);
            nome3.gameObject.SetActive(true);
            nome4.gameObject.SetActive(true);
        }

        menu.dadosPartida.numJogadores = value;
    }

    public void EscolherFacil() {
        menu.dadosPartida.dificuldade = Dificuldade.facil;

        Color cor = Color.white;
        cor.a = 0.1f;

        botaoMedio.color = cor;
        botaoDificil.color = cor;

        Color cor2 = Color.white;
        cor2.a = 0.5f;

        botaoFacil.color = cor2;
    }

    public void EscolherMedio() {
        menu.dadosPartida.dificuldade = Dificuldade.medio;

        Color cor = Color.white;
        cor.a = 0.1f;

        botaoFacil.color = cor;
        botaoDificil.color = cor;

        Color cor2 = Color.white;
        cor2.a = 0.5f;

        botaoMedio.color = cor2;
    }

    public void EscolherDificil() {
        menu.dadosPartida.dificuldade = Dificuldade.dificil;

        Color cor = Color.white;
        cor.a = 0.1f;

        botaoFacil.color = cor;
        botaoMedio.color = cor;

        Color cor2 = Color.white;
        cor2.a = 0.5f;

        botaoDificil.color = cor2;
    }

    public void IrParaAjustes() {
        painelAjustes.SetActive(true);
    }

    public void VoltarParaInicio() {
        painelAjustes.SetActive(false);
    }

    public void AtivarDesativarSomUI() {
        if (isSomAtivado.isOn) {
            menu.AtivarSom();
        } else {
            menu.DesativarSom();
        }
    }

    public void DesativarBotaoSom() {
        isSomAtivado.isOn = false;
    }

    public void IniciarJogo() {
        if (nome1.text != "") { menu.dadosPartida.nomes[0] = nome1.text; }
        if (nome2.text != "") menu.dadosPartida.nomes[1] = nome2.text;
        if (nome3.text != "") menu.dadosPartida.nomes[2] = nome3.text;
        if (nome4.text != "") menu.dadosPartida.nomes[3] = nome4.text;

        menu.IniciarJogo();
    }
}
