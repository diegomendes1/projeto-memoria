﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Som : MonoBehaviour{
    public AudioSource musica;
    public AudioSource somVitoria;
    public AudioSource somIncorreto;
    public AudioSource somCorreto;

    public bool somAtivado;

    public void AtivarSom() {
        somAtivado = true;
        musica.Play();
    }

    public void DesativarSom() {
        somAtivado = false;
        musica.Stop();
    }

    public void TocarVitoria() {
        if (somAtivado) {
            somVitoria.Play();
        }
    }

    public void TocarIncorreto() {
        if (somAtivado) {
            somIncorreto.Play();
        }
    }

    public void TocarCorreto() {
        if (somAtivado) {
            somCorreto.Play();
        }
    }
}
