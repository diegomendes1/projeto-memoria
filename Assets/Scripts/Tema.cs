﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tema : MonoBehaviour {
    public Sprite cartaEscondida;
    public Sprite[] cartasTipos;

    public Carta carta;

    public List<Carta> CriarCartas(int numCartas) {
        List<Carta> listaCartas = new List<Carta>();

        //mesmo i para cada carta igual.
        for (int i = 0; i < numCartas/2; i++) {
            Carta novaCarta1 = Instantiate(carta.gameObject).GetComponent<Carta>();
            novaCarta1.tema = this;
            novaCarta1.idCarta = i;
            novaCarta1.EsconderCarta();

            Carta novaCarta2 = Instantiate(carta.gameObject).GetComponent<Carta>();
            novaCarta2.tema = this;
            novaCarta2.idCarta = i;
            novaCarta2.EsconderCarta();

            listaCartas.Add(novaCarta1.GetComponent<Carta>());
            listaCartas.Add(novaCarta2.GetComponent<Carta>());
        }

        return listaCartas;
    }
}
