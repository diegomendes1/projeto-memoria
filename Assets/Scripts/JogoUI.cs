﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class JogoUI : MonoBehaviour {
    public Jogo jogo;

    private Carta primeiraCarta;
    private Carta segundaCarta;

    public bool esperando;

    [Header("Painel Jogadores")]
    public GameObject[] objs;
    public Text[] nomes;
    public Text[] pontos;

    [Space(10)]
    public Text textoJogadorAtual;

    [Header("Painel Fim de Jogo")]
    public GameObject painelFimJogo;
    public Text textoVitoria;

    void Start() {
        painelFimJogo.SetActive(false);    
    }

    public void MostrarCarta(Carta carta) {
        if (esperando) {
            return;
        }

        if (primeiraCarta == null) {
            primeiraCarta = carta;
        } else {
            if (carta == primeiraCarta) {
                return;
            }
            segundaCarta = carta;
            StartCoroutine(ChamarVerificacao(carta));
        }

        carta.MostrarCarta();
    }

    IEnumerator ChamarVerificacao(Carta carta) {
        esperando = true;
        yield return new WaitForSeconds(1);
        jogo.VerificarCartas(primeiraCarta, segundaCarta);

    }

    public void RetirarDuplaTabuleiro() {
        primeiraCarta.RetirarCarta();
        segundaCarta.RetirarCarta();
        primeiraCarta = null;
        segundaCarta = null;

        esperando = false;
    }

    public void EsconderDuplaTabuleiro() {
        primeiraCarta.EsconderCarta();
        segundaCarta.EsconderCarta();
        primeiraCarta = null;
        segundaCarta = null;

        esperando = false;
    }

    public void MostrarJogadoresCorretos(int numJogadores) {
        if (numJogadores == 1) {
            objs[1].SetActive(false);
            objs[2].SetActive(false);
            objs[3].SetActive(false);
        } else if (numJogadores == 2) {
            objs[2].SetActive(false);
            objs[3].SetActive(false);
        } else if (numJogadores == 3) {
            objs[3].SetActive(false);
        }
    }

    public void MostrarJogadorAtual(string nomeJogadorAtual) {
        textoJogadorAtual.text = "Jogue, " + nomeJogadorAtual;
    }

    public void MostrarFimJogo(string jogadorVencedor) {
        painelFimJogo.SetActive(true);

        textoVitoria.text = jogadorVencedor + " Ganhou!";
    }

    public void SairJogo() {
        jogo.SairJogo();
    }

    public void ReiniciarJogo() {
        jogo.ReiniciarJogo();
    }
}
