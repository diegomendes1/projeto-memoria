﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Jogo : MonoBehaviour {
    public JogoUI jogoUI;
    public Som som;

    public DadosPartida dadosPartida;
    public List<Carta> cartasNoJogo;
    public Transform painelCartas;

    public List<Jogador> jogadores;
    public List<int> pontosJogadores;

    public int jogadorAtual = -1;
    public GameObject jogadorPrefab;

    public int paresFaltando;

    void Awake() {
        som = GameObject.Find("Som").GetComponent<Som>();

        dadosPartida = GameObject.FindGameObjectWithTag("DadosPartida").GetComponent<DadosPartida>();
        cartasNoJogo = dadosPartida.tema.CriarCartas((int)dadosPartida.dificuldade);

        AdicionarCartasNoJogo();
        jogoUI.MostrarJogadoresCorretos(dadosPartida.numJogadores);
        ProximoJogador();
    }

    public void AdicionarCartasNoJogo() {
        paresFaltando = (int)dadosPartida.dificuldade / 2;

        List<Carta> cartasParaAdicionar = new List<Carta>();

        for(int i = cartasNoJogo.Count-1; i >= 0; i--) {
            Carta cartaAleatoria = cartasNoJogo[Random.Range(0, i)];

            cartaAleatoria.transform.SetParent(painelCartas);
            cartaAleatoria.transform.localScale = new Vector3(1, 1, 1);
            cartaAleatoria.jogoUI = jogoUI;

            cartasParaAdicionar.Add(cartaAleatoria);
            cartasNoJogo.Remove(cartaAleatoria);
        }

        for (int i = 0; i < dadosPartida.numJogadores; i++) {
            GameObject novo = Instantiate(jogadorPrefab);
            Jogador novoJogador = novo.GetComponent<Jogador>();
            novoJogador.nomeJogador = jogoUI.nomes[i];
            novoJogador.pontosJogador = jogoUI.pontos[i];

            novoJogador.nomeJogador.text = dadosPartida.nomes[i];
            novoJogador.pontosJogador.text = "0";

            jogadores.Add(novoJogador);
            pontosJogadores.Add(0);
        }

        cartasNoJogo = cartasParaAdicionar;
    }

    public void VerificarCartas(Carta a, Carta b) {
        if (a.idCarta == b.idCarta) {
            //Mesmo tipo de carta, achou
            PontuarJogador();
            jogoUI.RetirarDuplaTabuleiro();
            som.TocarCorreto();
        } else {
            //Errou, passa para outro jogador
            jogoUI.EsconderDuplaTabuleiro();
            ProximoJogador();
            som.TocarIncorreto();
        }
    }

    public void ProximoJogador() {
        jogadorAtual++;
        if (jogadorAtual == dadosPartida.numJogadores) {
            jogadorAtual = 0;
        }

        jogoUI.MostrarJogadorAtual(dadosPartida.nomes[jogadorAtual]);
    }

    public void PontuarJogador() {
        pontosJogadores[jogadorAtual]++;
        jogadores[jogadorAtual].pontosJogador.text = pontosJogadores[jogadorAtual].ToString();
        paresFaltando--;
        ChecarFimJogo();
    }

    public void ChecarFimJogo() {
        if (paresFaltando == 0) {
            int melhorJogador = -1;
            int melhorPontuacao = -1;

            for (int i = 0; i < jogadores.Count; i++) {
                if (pontosJogadores[i] > melhorPontuacao) {
                    melhorPontuacao = pontosJogadores[i];
                    melhorJogador = i;
                }
            }

            FimJogo(melhorJogador);
        }
    }

    public void FimJogo(int jogadorVencedor) {
        jogoUI.MostrarFimJogo(dadosPartida.nomes[jogadorVencedor]);
        som.TocarVitoria();
    }

    public void SairJogo() {
        Destroy(som.gameObject);
        Destroy(dadosPartida.gameObject);
        SceneManager.LoadScene(0);
    }

    public void ReiniciarJogo() {
        SceneManager.LoadScene(1);
    }
}
