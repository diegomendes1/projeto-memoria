﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Dificuldade {
    facil = 10,
    medio = 16,
    dificil = 24
}

public class DadosPartida : MonoBehaviour {
    public Dificuldade dificuldade;
    public int numJogadores;
    public Tema tema;

    public string[] nomes;
}
