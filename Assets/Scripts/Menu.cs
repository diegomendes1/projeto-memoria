﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour{
    public DadosPartida dadosPartida;
    public Som som;
    public MenuUI menuUi;

    public Tema[] temas;

    public void IniciarJogo() {
        GameObject.DontDestroyOnLoad(dadosPartida.gameObject);
        SceneManager.LoadScene(1);
    }

    void Start() {
        GameObject.DontDestroyOnLoad(som.gameObject);

        if (PlayerPrefs.HasKey("SomAtivado")) {
            if (PlayerPrefs.GetInt("SomAtivado") == 0) {
                menuUi.DesativarBotaoSom();
                DesativarSom();
            } else {
                AtivarSom();
            }
        } else {
            AtivarSom();
        }
    }

    public void DesativarSom() {
        PlayerPrefs.SetInt("SomAtivado", 0);
        som.DesativarSom();
    }

    public void AtivarSom() {
        PlayerPrefs.SetInt("SomAtivado", 1);
        som.AtivarSom();
    }
}
